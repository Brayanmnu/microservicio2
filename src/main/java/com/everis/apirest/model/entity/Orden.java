package com.everis.apirest.model.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Orden {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idOrden;
	@Column
	private Long clienteIdCliente;
	@Column
	private Long vendedorIdVendedor;
	@Column
	private String fechaOrden;
	@Column
	private BigDecimal total;
	@Column
	private BigDecimal igv; 
	@Column
	private Boolean activo = true; 
}
