package com.everis.apirest.model.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.everis.apirest.model.entity.Orden;
@Repository
public interface OrdenRepository extends CrudRepository<Orden, Long>{
	
	public Optional<Orden> findById(Long id);
}
