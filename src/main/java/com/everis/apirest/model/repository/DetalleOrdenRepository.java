package com.everis.apirest.model.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.everis.apirest.model.entity.DetalleOrden;

@Repository
public interface DetalleOrdenRepository extends CrudRepository<DetalleOrden, Long>{

	public Optional<DetalleOrden> findById(Long id);
}
