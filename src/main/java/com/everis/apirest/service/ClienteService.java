package com.everis.apirest.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.everis.apirest.controller.resource.ClienteResource;

@FeignClient("CLIENTE")
public interface ClienteService {
	
	@GetMapping("cliente/{id}")
	public ClienteResource obtenerClientePorId(@PathVariable("id") Long id) throws Exception;

}
