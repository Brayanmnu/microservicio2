package com.everis.apirest.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.everis.apirest.controller.resource.VendedorProductoResource;
import com.everis.apirest.controller.resource.VendedorResource;

@FeignClient("MICROSERVICIO1")
public interface MicroServicio1Service {

	@GetMapping("vendedor/{id}")
	public VendedorResource obtenerVendedorPorId(@PathVariable("id") Long id) throws Exception;
	
	@GetMapping("vendedorProducto/{id}")
	public VendedorProductoResource obtenerVendedorProductoPorId(@PathVariable("id") Long id) throws Exception;
}
