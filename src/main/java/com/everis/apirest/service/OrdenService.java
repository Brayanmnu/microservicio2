package com.everis.apirest.service;

import com.everis.apirest.model.entity.Orden;

public interface OrdenService {

	public Orden obtenerOrdenPorId(Long id) throws Exception;
	public Orden crearOrden (Orden request) throws Exception;
}
