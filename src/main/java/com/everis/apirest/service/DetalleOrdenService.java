package com.everis.apirest.service;

import com.everis.apirest.model.entity.DetalleOrden;

public interface DetalleOrdenService {
	public DetalleOrden obtenerDetalleOrdenPorId(Long id) throws Exception;
	public DetalleOrden crearDetalleOrde(DetalleOrden request) throws Exception;
	public Iterable<DetalleOrden> obtenerDetalleOrden();
}
