package com.everis.apirest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.everis.apirest.controller.resource.VendedorProductoResource;
import com.everis.apirest.model.entity.DetalleOrden;
import com.everis.apirest.model.repository.DetalleOrdenRepository;

@Service
public class DetalleOrdenServiceImpl implements DetalleOrdenService{

	@Autowired
	private DetalleOrdenRepository detalleOrdenRepository;
	
	
	@Override
	public DetalleOrden obtenerDetalleOrdenPorId(Long id) throws Exception {
		return detalleOrdenRepository.findById(id).orElseThrow(()->new Exception("Detalle de la orden no encontrada"));
	}
	
	@Override
	public DetalleOrden crearDetalleOrde(DetalleOrden request) throws Exception {
		return detalleOrdenRepository.save(request);
	}
	
	@Override
	public Iterable<DetalleOrden> obtenerDetalleOrden() {
		return detalleOrdenRepository.findAll();
	}
	
}
