package com.everis.apirest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.everis.apirest.model.entity.Orden;
import com.everis.apirest.model.repository.OrdenRepository;

@Service
public class OrdenServiceImpl implements OrdenService{
	
	@Autowired
	private OrdenRepository ordenRepository;
	@Override
	public Orden crearOrden(Orden request) throws Exception{
		return ordenRepository.save(request);
	}
	@Override
	public Orden obtenerOrdenPorId(Long id) throws Exception {
		return ordenRepository.findById(id).orElseThrow(()->new Exception("Orden no encontrada"));
	}
}
