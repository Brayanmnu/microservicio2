package com.everis.apirest.controller;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.everis.apirest.controller.resource.DetalleOrdenReducidoResource;
import com.everis.apirest.controller.resource.DetalleOrdenResource;
import com.everis.apirest.controller.resource.OrdenResource;
import com.everis.apirest.model.entity.DetalleOrden;
import com.everis.apirest.model.entity.Orden;
import com.everis.apirest.service.ClienteService;
import com.everis.apirest.service.DetalleOrdenService;
import com.everis.apirest.service.MicroServicio1Service;
import com.everis.apirest.service.OrdenService;

@RestController
public class ApiController {
	
	@Autowired
	DetalleOrdenService detalleOrdenService;
	
	@Autowired
	OrdenService ordenService;
	
	@Autowired
	ClienteService clienteService;
	
	@Autowired
	MicroServicio1Service microServicio1Service;
	
	@PostMapping("/orden")
	public ResponseEntity<DetalleOrdenResource> crearOrden(@RequestBody DetalleOrdenReducidoResource request) throws Exception {

		DetalleOrden detalleOrden = new DetalleOrden();
		Orden orden = new Orden();
		BigDecimal bigDecimal = new BigDecimal(0.18);
		Date ahora = new Date();
		SimpleDateFormat formateador = new SimpleDateFormat("dd-MM-yyyy");
		
		orden.setClienteIdCliente(request.getOrden().getIdCliente());
		orden.setFechaOrden(formateador.format(ahora));
		orden.setIgv(bigDecimal);
		orden.setTotal(request.getPrecioUnitario().multiply(bigDecimal.add(new BigDecimal(1))));
		orden.setVendedorIdVendedor(request.getOrden().getIdVendedor());
		Orden ordenOut = ordenService.crearOrden(orden);
		
		OrdenResource ordenResource = new OrdenResource();
		ordenResource.setIdOrden(ordenOut.getIdOrden());
		ordenResource.setCliente(clienteService.
				obtenerClientePorId(ordenOut.getClienteIdCliente()));
		ordenResource.setVendedor(microServicio1Service
				.obtenerVendedorPorId(ordenOut.getVendedorIdVendedor()));

		ordenResource.setFechaOrden(ordenOut.getFechaOrden());
		ordenResource.setTotal(ordenOut.getTotal());
		ordenResource.setIgv(ordenOut.getIgv());
		ordenResource.setActivo(ordenOut.getActivo());
		
		
		detalleOrden.setCantidad(request.getCantidad());
		detalleOrden.setOrden(orden);
		detalleOrden.setPrecioUnitario(request.getPrecioUnitario());
		detalleOrden.setVendedorProductoIdVendedorProducto(request.getIdVendedorProducto());
		DetalleOrden detalleOrdenOut = detalleOrdenService.crearDetalleOrde(detalleOrden);
		
		DetalleOrdenResource detalleOrdenResource = new DetalleOrdenResource();
		detalleOrdenResource.setIdDetalleOrden(detalleOrdenOut.getIdDetalleOrden());
		
		detalleOrdenResource.setVendedorProducto(microServicio1Service
				.obtenerVendedorProductoPorId(detalleOrdenOut
						.getVendedorProductoIdVendedorProducto()));
					
		detalleOrdenResource.setOrden(ordenResource);
		detalleOrdenResource.setCantidad(detalleOrdenOut.getCantidad());
		detalleOrdenResource.setPrecioUnitario(detalleOrdenOut.getPrecioUnitario());
		detalleOrdenResource.setActivo(detalleOrdenOut.getActivo());
		
		
		return  new ResponseEntity<>(detalleOrdenResource, HttpStatus.CREATED);
	}
	

	@GetMapping("/ordenes")
	public List<DetalleOrdenResource> obtenerOrdenes() {
		List<DetalleOrdenResource> listado = new ArrayList<>(); 
		detalleOrdenService.obtenerDetalleOrden().forEach(detalleOrden->{
			DetalleOrdenResource detalleOrdenResource = new DetalleOrdenResource();
			OrdenResource ordenResource = new OrdenResource();
			
			ordenResource.setActivo(detalleOrden.getOrden().getActivo());
			try {
				ordenResource.setCliente(clienteService.obtenerClientePorId( detalleOrden
						.getOrden().getClienteIdCliente()));
			} catch (Exception e) {
				e.printStackTrace();
			}
			ordenResource.setFechaOrden(detalleOrden.getOrden().getFechaOrden());
			ordenResource.setIdOrden(detalleOrden.getOrden().getIdOrden());
			ordenResource.setIgv(detalleOrden.getOrden().getIgv());
			ordenResource.setTotal(detalleOrden.getOrden().getTotal());
			try {
				ordenResource.setVendedor(microServicio1Service
						.obtenerVendedorPorId(detalleOrden.getOrden().getVendedorIdVendedor()));
			} catch (Exception e) {
				e.printStackTrace();
			}
			detalleOrdenResource.setOrden(ordenResource);
			detalleOrdenResource.setPrecioUnitario(detalleOrden.getPrecioUnitario());
			detalleOrdenResource.setActivo(detalleOrden.getActivo());
			detalleOrdenResource.setCantidad(detalleOrden.getCantidad());
			detalleOrdenResource.setIdDetalleOrden(detalleOrden.getIdDetalleOrden());
			try {
				detalleOrdenResource.setVendedorProducto(microServicio1Service
						.obtenerVendedorProductoPorId(detalleOrden
								.getVendedorProductoIdVendedorProducto()));
			} catch (Exception e) {
				e.printStackTrace();
			}
			listado.add(detalleOrdenResource);
		});
		return listado;
	}
	
	@GetMapping("orden/{id}")
	public OrdenResource obtenerOrdenes(@PathVariable("id") Long id) throws Exception{
		Orden ordenOut = ordenService.obtenerOrdenPorId(id);
		OrdenResource ordenResource = new OrdenResource();
		ordenResource.setActivo(ordenOut.getActivo());
		ordenResource.setCliente(clienteService.obtenerClientePorId(ordenOut
				.getClienteIdCliente()));
		ordenResource.setFechaOrden(ordenOut.getFechaOrden());
		ordenResource.setIdOrden(ordenOut.getIdOrden());
		ordenResource.setIgv(ordenOut.getIgv());
		ordenResource.setTotal(ordenOut.getTotal());
		ordenResource.setVendedor(microServicio1Service
				.obtenerVendedorPorId(ordenOut.getVendedorIdVendedor()));
		return ordenResource;
	}
	
	@GetMapping("detalleOrden/{id}")
	public DetalleOrdenResource obtenerDetalleOrden(@PathVariable("id")Long id) throws Exception{
		DetalleOrden detalleOrden = detalleOrdenService.obtenerDetalleOrdenPorId(id);
		DetalleOrdenResource detalleOrdenResource = new DetalleOrdenResource();
		detalleOrdenResource.setActivo(detalleOrden.getActivo());
		detalleOrdenResource.setCantidad(detalleOrden.getCantidad());
		detalleOrdenResource.setIdDetalleOrden(detalleOrden.getIdDetalleOrden());
		Orden orden = ordenService.obtenerOrdenPorId(detalleOrden.getOrden().getIdOrden());
		OrdenResource ordenResource = new OrdenResource();
		ordenResource.setActivo(orden.getActivo());
		ordenResource.setCliente(clienteService
				.obtenerClientePorId(orden.getClienteIdCliente()));
		ordenResource.setFechaOrden(orden.getFechaOrden());
		ordenResource.setIdOrden(orden.getIdOrden());
		ordenResource.setIgv(orden.getIgv());
		ordenResource.setTotal(orden.getTotal());
		ordenResource.setVendedor(microServicio1Service
				.obtenerVendedorPorId(orden.getVendedorIdVendedor()));
		detalleOrdenResource.setOrden(ordenResource);
		detalleOrdenResource.setPrecioUnitario(detalleOrden.getPrecioUnitario());
		detalleOrdenResource.setVendedorProducto(microServicio1Service
				.obtenerVendedorProductoPorId(detalleOrden
						.getVendedorProductoIdVendedorProducto()));
		return detalleOrdenResource;
	}
}
