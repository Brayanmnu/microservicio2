package com.everis.apirest.controller.resource;

import lombok.Data;

@Data
public class OrdenReducidoResource {
	private Long idCliente;
	private Long idVendedor;
}
