package com.everis.apirest.controller.resource;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class DetalleOrdenResource {
	private Long idDetalleOrden;
	private VendedorProductoResource vendedorProducto;
	private OrdenResource orden;
	private BigDecimal cantidad;
	private BigDecimal precioUnitario;
	private Boolean activo;
}
