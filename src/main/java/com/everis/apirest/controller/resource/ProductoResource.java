package com.everis.apirest.controller.resource;

import lombok.Data;

@Data
public class ProductoResource {
	private Long idProducto;
	private String descripcion;
	private String nombre;
	private String unidadMedida;
}
