package com.everis.apirest.controller.resource;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class DetalleOrdenReducidoResource {
	private Long idVendedorProducto;
	private OrdenReducidoResource orden;
	private BigDecimal cantidad;
	private BigDecimal precioUnitario;
}
