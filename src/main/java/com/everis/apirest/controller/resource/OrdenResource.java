package com.everis.apirest.controller.resource;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class OrdenResource {
	private Long idOrden;
	private ClienteResource cliente;
	private VendedorResource vendedor;
	private String fechaOrden;
	private BigDecimal total;
	private BigDecimal igv; 
	private Boolean activo;
}
