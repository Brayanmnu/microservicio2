package com.everis.apirest.controller.resource;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class VendedorProductoResource {
	private Long idVendedorProducto;
	private BigDecimal precio;
	private ProductoResource producto;
	private VendedorResource vendedor;
}
