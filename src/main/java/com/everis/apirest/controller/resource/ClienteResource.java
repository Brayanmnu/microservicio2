package com.everis.apirest.controller.resource;

import lombok.Data;

@Data
public class ClienteResource {
	private Long idCliente;
	private String nombres;
	private String apellidos;
	private String direccion;
}
